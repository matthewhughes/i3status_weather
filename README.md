# i3status_weather

Get the current weather from openweathermap.org and print it in an i3status
friendly manner

## Usage

```
i3status_weather [-h] [-c CITY] [-u {metric,imperial,standard}] -k OPEN_WEATHER_KEY

  -h, --help            show help message and exit
  -c CITY, --city CITY  The city to lookup weather for, if not provided
                        attempt to find via ip-api.com
  -u, --units {metric,imperial,standard}
                        Units to be used by Open Weather, defaults to 'metric'
  -k OPEN_WEATHER_KEY, --open-weather-key OPEN_WEATHER_KEY
                        API key (APPID) for openweathermap.org
```
